from math import *


def inputofnum():
    global num1
    global num2
    num1 = float(input("Enter number one : "))
    num2 = float(input("Enter number two : "))


inputofnum()
def epichackingulator():
    operation=input("Enter the operation needed (Enter \"None\" to abort, operations : + - * / sqrt ceil floor) : ")
    if operation == "+":
        sum = num1 + num2
        print(sum)
        ask=input("Would you like to perform another operation? (Yes/No) : ")
        if ask == "Yes":
            return inputofnum()
        elif ask == "No":
            exit()
    elif operation == "-":
        choice=input("Would you like to subtract num1 from num2 (option 1) or num2 from num1 (option2) [1/2] : ")
        if choice == "1":
            difference = num2 - num1
            print(difference)
            ask=input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
        elif choice == "2":
            difference= num1 - num2
            print(difference)
            ask=input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
    elif operation == "*":
        prod = num1 * num2
        print(prod)
        ask = input("Would you like to perform another operation? (Yes/No) : ")
        if ask == "Yes":
            return inputofnum()
        elif ask == "No":
            exit()
    elif operation == "/":
        choice=input("Divide num1/num2 or num2/num1? [1/2] : ")
        if choice == "1":
            quoti = num1/num2
            print(quoti)
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
        elif choice == "2":
            quoti = num2/num1
            print(quoti)
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
    elif operation == "sqrt":
        choice = input("Which number would you like to find the square root of? [1/2/Both] : ")
        if choice == "1":
            print(sqrt(num1))
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
        elif choice == "2":
            print(sqrt(num2))
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
        elif choice == "Both":
            print(sqrt(num1))
            print(sqrt(num2))
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
    elif operation == "ceil":
        choice = input("Which number would you like to find the ceiling function of? [1/2/Both] : ")
        if choice == "1":
            print(ceil(num1))
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
        elif choice == "2":
            print(ceil(num2))
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
        elif choice == "Both":
            print(ceil(num1))
            print(ceil(num2))
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
    elif operation == "floor":
        choice = input("Which number would you like to find the floor function of? [1/2/Both] : ")
        if choice == "1":
            print(floor(num1))
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
        elif choice == "2":
            print(floor(num2))
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()
        elif choice == "Both":
            print(floor(num1))
            print(floor(num2))
            ask = input("Would you like to perform another operation? (Yes/No) : ")
            if ask == "Yes":
                return inputofnum()
            elif ask == "No":
                exit()

while True :
    epichackingulator()
