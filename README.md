# epic-calck

A simple calculator written in Python, using the Qt framework.

# CLI calck

CLI calck doesn't use the Qt framework, and should work as long as Python 3.11+ supports your operating system.

# System Requirements
- Python 3.11+
- Ability to use the `math` library.
